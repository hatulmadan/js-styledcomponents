import React from 'react'
import {render} from 'react-dom'
import LightWrapped from './components/Light/LightWrapped'

render(<LightWrapped/>, document.getElementById('root'))
