import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const AlertText = styled.text`
    color: #f07300;
`;

export default AlertText