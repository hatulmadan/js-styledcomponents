import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import SuccessText from './SuccessText'

class SuccessTextWrapped extends Component {
    render() {
        return(
            <SuccessText>
                {this.props.text}
            </SuccessText>
        )
    }
}

export default SuccessTextWrapped