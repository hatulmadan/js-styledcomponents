import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import InfoText from './InfoText'

class InfoTextWrapped extends Component {
    render() {
        return(
            <InfoText>
                {this.props.text}
            </InfoText>
        )
    }
}

export default InfoTextWrapped