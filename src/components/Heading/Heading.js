import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const Heading = styled.h1`
    font-family: SFProDisplay;
    font-size: 24px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: 0.5px;
    color: #0a1f44;
`;

export default Heading