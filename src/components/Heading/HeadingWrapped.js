import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import Heading from './Heading'

class HeadingWrapped extends Component {
    render() {
        return(
            <Heading>
                {this.props.title} //not sure here
            </Heading>
        )
    }
}

export default HeadingWrapped