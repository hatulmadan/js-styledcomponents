import React, {Components} from 'react'
import styled, {css} from 'styled-components'

// This could be react-router's Link for example
const Link = ({ className, children }) => (
    <a className={className}>
        {children}
    </a>
)

const StyledLink = styled(Link)`
  color: palevioletred;
  font-weight: bold;
`;

render(
    <div>
        <Link>Unstyled, boring Link</Link>
        <br />
        <StyledLink>Styled, exciting Link</StyledLink>
    </div>
);
