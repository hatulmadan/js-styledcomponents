import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import Regular from './Regular'

class RegularWrapped extends Component {
    render() {
        return(
            <Regular>
                {this.props.text}
            </Regular>
        )
    }
}

export default RegularWrapped