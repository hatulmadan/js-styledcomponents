import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import Bold from './Bold'

class BoldWrapped extends Component {
    render() {
        return(
            <Bold>
                {this.props.text}
            </Bold>
        )
    }
}

export default BoldWrapped