import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import ErrorText from './ErrorText'

class ErrorTextWrapped extends Component {
    render() {
        return(
            <ErrorText>
                {this.props.text}
            </ErrorText>
        )
    }
}

export default ErrorTextWrapped