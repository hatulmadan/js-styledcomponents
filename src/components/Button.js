import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const Button = styled.button`
    border-radius: 4px;
    width: 136px;
    height: 48px;
    background-color: rgba(255, 255, 255, 0);
`;

export default Button