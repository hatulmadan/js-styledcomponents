import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const InfoText = styled.text`
    color: #0d55cf;
`;

export default InfoText