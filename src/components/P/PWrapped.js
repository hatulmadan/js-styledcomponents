import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import P from './P'

class PWrapped extends Component {
    render() {
        return(
            <P>
                {this.props.text}
            </P>
        )
    }
}

export default PWrapped