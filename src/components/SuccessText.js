import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const SuccessText = styled.text`
    color: #00865a;
`;

export default SuccessText