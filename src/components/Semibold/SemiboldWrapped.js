import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import Semibold from './Semibold'

class SemiboldWrapped extends Component {
    render() {
        return(
            <Semibold>
                {this.props.text}
            </Semibold>
        )
    }
}

export default SemiboldWrapped