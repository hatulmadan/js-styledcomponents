import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const Semibold = styled.text`
    font-family: SFProText;
    font-size: 15px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.07;
    letter-spacing: normal;
    color: #53627c;
`;

export default Semibold