import React, {Component} from 'react'
import styled, {css} from 'styled-components'
import Light from './Light'

class LightWrapped extends Component {
    render() {
        return(
            <Light>
                {this.props.text}
            </Light>
        )
    }
}

export default LightWrapped