import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const Light = styled.text`
    font-family: SFProText;
    font-size: 15px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.07;
    letter-spacing: normal;
    color: #53627c;
`;

export default Light