import React, {Components} from 'react'
import styled, {css} from 'styled-components'

const ErrorText = styled.text`
    color: #dd2727;
`;

export default ErrorText